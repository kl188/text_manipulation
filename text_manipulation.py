def capitalize_women(fileRead, fileWrite):
    """ replaces each instance of 'women' with 'WOMEN'

    :param fileRead: name of file to read in
    :param fileWrite: name of file to write to
    :return: file with replaced instances
    """
    uncap = open(fileRead, 'r')
    cap = open(fileWrite, 'w')

    a = uncap.readline()
    cap.write(a.replace('women', 'WOMEN'))

    uncap.close()
    cap.close()

    return cap

def count_men(fileRead):
    """ counts the number of instances of 'men'

    :param fileRead: name of file to read in
    :return: number of instances of 'men'
    """
    f = open(fileRead, 'r')
    count = 0
    a = f.readlines()
    for line in a:
        words = line.split()
        for word in words:
            if (word == 'men'):
                count += 1

    f.close()
    return count

def contains_blue_devil(fileRead):
    """ test if file contains the words 'Blue Devil'

    :param fileRead: name of file to read in
    :return: true if file contains 'Blue Devil', false if it doesn't
    """

    f = open(fileRead, 'r')
    a = f.readlines()
    for line in a:
        if (line.find('Blue Devil') >= 0):
            return True
    return False

if __name__ == '__main__':
    a = capitalize_women('women_example_text.txt', 'example_text_new.txt')
    b = count_men('women_example_text.txt')
    c = contains_blue_devil('women_example_text.txt')